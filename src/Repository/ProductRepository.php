<?php

namespace App\Repository;

use App\Entity\Product;
use App\Filter\FilterProduct;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Product::class);
        $this->paginator = $paginator;
    }

    public function findSearch(FilterProduct $search): PaginationInterface {
        $query = $this
            ->createQueryBuilder('p');

        if (!empty($search->getQ())) {
            $query = $query
                ->andWhere('p.name LIKE :q')
                ->setParameter('q', "%{$search->getQ()}%");
        }

        if (!empty($search->getSupplier())) {
            $query = $query
                ->andWhere('p.supplier LIKE :supplier')
                ->setParameter('supplier', "%{$search->getSupplier()}%");
        }

        if (!empty($search->getMinPrice())) {
            $query = $query
                ->andWhere('p.price >= :min')
                ->setParameter('min', $search->getMinPrice());
        }

        if (!empty($search->getMaxPrice())) {
            $query = $query
                ->andWhere('p.price <= :max')
                ->setParameter('max', $search->getMaxPrice());
        }

        return $this->paginator->paginate(
            $query,
            $search->page,
            6
        );
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
