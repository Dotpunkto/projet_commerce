<?php

namespace App\Repository;

use App\Entity\SaleProduct;
use App\Filter\FilterSale;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method SaleProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method SaleProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method SaleProduct[]    findAll()
 * @method SaleProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SaleProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, SaleProduct::class);
        $this->paginator = $paginator;
    }

    public function findSearch(FilterSale $search): PaginationInterface {
        $query = $this
            ->createQueryBuilder('p');

        if (!empty($search->getColor())) {
            $query = $query
                ->andWhere('p.color LIKE :color')
                ->setParameter('color', "%{$search->getColor()}%");
        }

        if (!empty($search->getQuantityMin())) {
            $query = $query
                ->andWhere('p.quantity >= :quantityMin')
                ->setParameter('quantityMin', $search->getQuantityMin());
        }

        if (!empty($search->getQuantityMax())) {
            $query = $query
                ->andWhere('p.quantity <= :quantityMax')
                ->setParameter('quantityMax', $search->getQuantityMax());
        }
        /*
        if (!empty($search->amountMin)) {
            $query = $query
                ->andWhere('sale.amount <= :amountMin')
                ->setParameter('amountMin', $search->amountMin);
        }

        if (!empty($search->amountMax)) {
            $query = $query
                ->andWhere('sale.amount <= :amountMax')
                ->setParameter('amountMax', $search->amountMax);
        }*/

        return $this->paginator->paginate(
            $query,
            $search->page,
            10
        );
    }

    // /**
    //  * @return SaleProduct[] Returns an array of SaleProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SaleProduct
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
