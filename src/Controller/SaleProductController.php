<?php

namespace App\Controller;

use App\Filter\FilterSale;
use App\Entity\SaleProduct;
use App\Form\SearchSaleForm;
use App\Repository\TagRepository;
use App\Repository\SaleProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/sale/product")
 */
class SaleProductController extends AbstractController
{
    /**
     * @Route("/", name="sale_product_index", methods={"GET"})
     */
    public function index(SaleProductRepository $SaleProductrepository, TagRepository $tagRepository, Request $request): Response
    {
        $search = new FilterSale();
        $search->page = $request->get('page', 1);
        $form = $this->createForm(SearchSaleForm::class, $search);
        $form->handleRequest($request);
        $sales = $SaleProductrepository->findSearch($search);
        $tags = $tagRepository->findAll();
        return $this->render('sale_product/index.html.twig', [
            'active' => 'Sale',
            'search' => $form->createView(),
            'sales' => $sales,
            'tags' => $tags
        ]);
    }

    /**
     * @Route("/{id}", name="sale_product_show", methods={"GET"})
     */
    public function show(SaleProduct $saleProduct, TagRepository $tagRepository): Response
    {
        $product = $saleProduct->getProduct();
        $id = $product->getId();
        $tag = $tagRepository->find($id);
        return $this->render('sale_product/show.html.twig', [
            'active' => 'Sale',
            'sale_product' => $saleProduct,
            'tag' => $tag
        ]);
    }
}
