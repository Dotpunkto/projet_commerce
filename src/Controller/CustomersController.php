<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Customer;
use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\CustomerType;
use App\Repository\CustomerRepository;


/**
 * @Route("/customer")
 */
class CustomersController extends AbstractController
{
    /**
     * @Route("/customers", name="customers")
     */
   /* public function index()
    {
        return $this->render('customers/index.html.twig', [
            'controller_name' => 'CustomersController',
        ]);
    }*/

/**
 * @Route("/display", name="display")
 */

    public function display_customer()
    { 
        $repo = $this->getDoctrine()->getRepository(customer::class);

        $users = $repo->findAll();
     
        return $this->render('customers/display.html.twig',[
            'active' => 'Customer',
            'users'=>$users
            
        ]);
    }

    /**
 * @Route("/product_customer", name="product_customer")
 */

public function product_customer()
{ 
    $repo = $this->getDoctrine()->getRepository(product::class);  

    $products = $repo->findAll();        
    
    return $this->render('customers/product_customer.html.twig',[
        'active' => 'Customer',
        'products'=>$products
    ]);
}
  /**
 * @Route("/new_customer", name="new_customers")
 */

/*public function new_customers()
{ 
return $this->render('customers/new.html.twig');
}*/


/**
     * @Route("/customer", name="customer_index", methods={"GET"})
     */
    public function index(CustomerRepository $customerRepository): Response
    {
        return $this->render('customers/index.html.twig', [
            'active' => 'Customer',
            'customers' => $customerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new_customer", name="customer_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $customer = new Customer();
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($customer);
            $entityManager->flush();

            return $this->redirectToRoute('customer_index');
        }

        return $this->render('customers/new.html.twig', [
            'active' => 'Customer_new',
            'customer' => $customer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="customer_show", methods={"GET"})
     */
    public function show(Customer $customer): Response
    {
        return $this->render('customers/show.html.twig', [
            'active' => 'Customer',
            'customer' => $customer,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="customer_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Customer $customer): Response
    {
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('customer_index');
        }

        return $this->render('customers/edit.html.twig', [
            'active' => 'Customer',
            'customer' => $customer,
            'form' => $form->createView(),
        ]);
    }

    

}
