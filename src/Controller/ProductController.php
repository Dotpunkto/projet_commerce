<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Form\TagType;
use App\Entity\Product;
use App\Form\SearchForm;
use App\Filter\FilterProduct;
use App\Repository\TagRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="product_index", methods={"GET"})
     */
    public function index(ProductRepository $repository, TagRepository $tagRepository, Request $request)
    {
        $search = new FilterProduct();
        $search->page = $request->get('page', 1);
        $form = $this->createForm(SearchForm::class, $search);
        $form->handleRequest($request);
        $products = $repository->findSearch($search);
        $tags = $tagRepository->findAll();
        return $this->render('product/index.html.twig', [
            'active' => 'Product',
            'products' => $products,
            'search' => $form->createView(),
            'tags' => $tags
        ]);
    }

    /**
     * @Route("/new", name="product_new", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function new(Request $request): Response
    {
        $tag = new Tag();
        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tag);
            $entityManager->flush();

            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/new.html.twig', [
            'active' => 'Product',
            'form' => $form->createView(),
        ]);
    }
    

    /**
     * @Route("/show/{id}", name="product_show", methods={"GET"})
     */
    public function show(Product $product, TagRepository $tagRepository): Response
    {
        $id = $product->getId();
        $tags = $tagRepository->findall();
        $tag = $this->getTag($tags, $id);
        return $this->render('product/show.html.twig', [
            'active' => 'Product',
            'product' => $product,
            'tag' => $tag->getName()
        ]);
    }

    private function getTag($tags, $id) {
        foreach ($tags as $key => $element) {
            if($element->getProduct()->getId() == $id) {
                return $element;
            }
        }
    }

    /**
     * @Route("/edit/{id}", name="product_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function edit(Request $request, Tag $tag): Response
    {
        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/edit.html.twig', [
            'active' => 'Product',
            'product' => $tag,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("remove/{id}", name="product_delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     */
    public function delete(Request $request, Product $product): Response
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();
        }

        return $this->redirectToRoute('product_index');
    }
}
