<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\Tag;
use App\Entity\Sale;
use App\Entity\User;
use App\Entity\Product;
use App\Entity\Customer;
use App\Entity\SaleProduct;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProductFixtures extends Fixture
{

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager) {

        $this->loadUser($manager);
        //$this->loadData($manager);
        $manager->flush();
    }

    private function loadUser($manager) {
        $user = new User();

        
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            '11111111'
        ));
        $user->setRoles(['ROLE_Admin']);
        $user->setEmail('coucou_je_suis_un_mail@osef.com');
        $user->setUsername('Osef');
        $manager->persist($user);
    }

    private function loadData($manager) {
        $faker = Faker\Factory::create('fr_FR');
        
        $products = $this->createProduct($faker, $manager);
        $this->createSaleProduct($faker, $manager, $products);
    }

    private function createProduct($faker, $manager):array {
        $tagTab = $faker->words(5, false);
        $tabCompany = $faker->words(10, false);
        $products = array();
        for ($i = 0; $i < 20; $i++) {
            $product = new Product();
            $product->setName($faker->word())
                    ->setPrice($faker->randomFloat($nbMaxDecimals = 2, $min = 0.01, $max = 1000))
                    ->setDescription($faker->text($maxNbChars = 200))
                    ->setSupplier($tabCompany[mt_rand(0,9)])
                    ->setWebSiteSupplier($faker->url());
            $tag = $this->createTag($product, $tagTab);
            array_push($products, $product);
            $manager->persist($product);
            $manager->persist($tag);
        }
        return $products;
    }

    private function createTag($product, $tagTab):Tag {
        $tag = new Tag();
        $tag->setName($tagTab[mt_rand(0,4)])
            ->setProduct($product);
        return $tag;
    }

    private function createSaleProduct($faker, $manager, $products) {
        for ($i = 0; $i < 50; $i++) {
            $saleProduct = new SaleProduct();
            $product = $products[mt_rand(0,19)];
            $quantity = mt_rand(1,5);
            $saleProduct->setQuantity($quantity)
                        ->setColor($faker->safeColorName())
                        ->setProduct($product)
                        ->setSale($this->createSale($faker, $manager, $product, $quantity));
            $manager->persist($saleProduct);
        }
    }

    private function createSale($faker, $manager, $product, $quantity):Sale {
        $sale = new Sale();
        $sale->setDateOfPurchase($faker->date())
            ->setAmount($product->getPriceProduct() * $quantity)
            ->setCustomer($this->createCustomer($manager, $faker));
        $manager->persist($sale);
        return $sale;
    }

    private function createCustomer($manager, $faker):Customer {
        $customer = new Customer();
        $customer->setFirstName($faker->firstName())
                ->setLastName($faker->lastName())
                ->setMail($faker->safeEmail())
                ->setPhone($faker->e164PhoneNumber())
                ->setAddress($faker->streetAddress())
                ->setCity($faker->city())
                ->setCountry($faker->country());
        $manager->persist($customer);
        return $customer;
    }
}
