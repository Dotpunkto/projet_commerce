<?php

namespace App\Form;

use App\Filter\FilterSale;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class SearchSaleForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder 
            ->add('color', TextType::class, [
                'label' => false, 
                'required' => false,
                'attr' => [
                    'placeholder' => 'Color'
                ]
            ])
            ->add('quantityMin', NumberType::class, [
                'allow_extra_fields' => true,
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Min quantity'
                ]
            ])
            ->add('quantityMax', NumberType::class, [
                'allow_extra_fields' => true,
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Max quantity'
                ]
            ])/*
            ->add('amountMin', NumberType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Min amount'
                ]
            ])
            ->add('amountMax', NumberType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Max amount'
                ]
            ])*/
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FilterSale::class,
            'method' => 'GET',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}