<?php

namespace App\Form;

use App\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('firstName',TextareaType::class,[
            'attr'=>[
            'placeholder'=>"Ecriver votre prénom",
            'class'=>"form-control"
            ],
            'required' => true
        ])
        ->add('lastName',TextareaType::class,[
            'attr'=>[
            'placeholder'=>"Ecriver votre nom",
            'class'=>"form-control"
            ],
            'required' => true
        ])
        ->add('mail',EmailType::class,[
            'attr'=>[
            'placeholder'=>"Ecriver votre mail",
            'class'=>"form-control"
            ],
            'required' => true
        ])
        ->add('phone',NumberType::class,[
            'attr'=>[
            'placeholder'=>"Inscriver votre téléphone",
            'class'=>"form-control"
            ]
        ])
        ->add('address',TextareaType::class,[
            'attr'=>[
            'placeholder'=>"Ecriver votre adresse",
            'class'=>"form-control"
            ]
        ])
        ->add('city', TextareaType::class,[
            'attr'=>[
            'placeholder'=>"Ecriver votre ville",
            'class'=>"form-control"
            ]
        ])
        ->add('country', TextareaType::class,[
            'attr'=>[
            'placeholder'=>"Ecriver votre pays",
            'class'=>"form-control"
            ]
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
