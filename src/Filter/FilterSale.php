<?php

namespace App\Filter;

class FilterSale {

    /**
     * @var null|integer
     */
    private $quantityMin;

    /**
     * @var null|integer
     */
    private $quantityMax;

    /**
     * @var null|string
     */
    private $color = '';

    /**
     * @var null|float
     */
    //public $amountMin;

    /**
     * @var null|float
     */
    //public $amountMax;

    public function getQuantityMin():?int {
        return $this->quantityMin;
    }

    public function setQuantityMin(?int $quantityMin) {
        $this->quantityMin = $quantityMin;
        return $this;
    }

    public function getQuantityMax():?int {
        return $this->quantityMax;
    }

    public function setQuantityMax(?int $quantityMax) {
        $this->quantityMax = $quantityMax;
        return $this;
    }

    public function getColor():?string {
        return $this->color;
    }

    public function setColor(?string $color) {
        $this->color = $color;
        return $this;
    }
}