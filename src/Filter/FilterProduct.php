<?php

namespace App\Filter;

class FilterProduct {

    /**
     * @var null|string
     */
    private $q = '';

    /**
     * @var null|float
     */
    private $minPrice;

    /**
     * @var null|float
     */
    private $maxPrice;

    /**
     * @var null|string
     */
    private $supplier = '';

    public function getQ():?string {
        return $this->q;
    }

    public function setQ(?string $q) {
        $this->q = $q;
        return $this;
    }

    public function getMinPrice():?float {
        return $this->minPrice;
    }

    public function setMinPrice(?float $minPrice) {
        $this->minPrice = $minPrice;
        return $this;
    }

    public function getMaxPrice():?float {
        return $this->maxPrice;
    }

    public function setMaxPrice(?float $maxPrice) {
        $this->maxPrice = $maxPrice;
        return $this;
    }

    public function getSupplier():?string {
        return $this->supplier;
    }

    public function setSupplier(?string $supplier) {
        $this->supplier = $supplier;
        return $this;
    }

}